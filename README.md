Common MVC Security 
____

В этом задании вам предстоит реализовать простой запрет на попадание на определённые URL неавторизованным пользователям.
____
В src/main/resources/templates лежат html файлы (View). Pom.xml тоже настроен для работы. В папке java лежит WebSecurityApplication, его менять нельзя.
____
Для начала Вам надо в классе MvcConfig добавить реализацию метода addViewControllers(ViewControllerRegistry registry) интерфейса WebMvcConfigurer со следующей связкой URL с View's (каталог template):
/ - home, /home - home, /hello - hello, /login - login.
____
После этого следует добавить реализацию методов void configure(HttpSecurity http) и UserDetailsService userDetailsService() класса WebSecurityConfigurerAdapter в классе WebSecurityConfig.
____
Примечание для реализаций функций: в userDetailsService должно использоваться следующее описание пользователя: user - "user", password = "password", roles = "USER".
____
Для функции configure: неавторизованные пользователи могут попадать только на / и /home URL, попытка доступа к другому URl должна приводить к redirect на login View, в котором следует 
вписать в форму user и password. После авторизации, пользователь попадает на ту страницу, которую запрашивал, даже если её не существует.
____
В src/test/java/ лежит класс с тестами, с помощью них Вы можете проверять работу программы до загрузки в Autocode.
